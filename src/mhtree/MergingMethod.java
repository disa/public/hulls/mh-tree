package mhtree;

import messif.objects.LocalAbstractObject;

import java.util.List;

public enum MergingMethod {
    /**
     * Every object in descendants leaf node's bucket is retrieved and returned.
     */
    OBJECT_BASED_MERGE {
        @Override
        public List<LocalAbstractObject> getObjects(Node node) {
            return node.getObjects();
        }
    },
    /**
     * Returns the hull objects in {@code node}'s hull.
     */
    HULL_BASED_MERGE {
        @Override
        public List<LocalAbstractObject> getObjects(Node node) {
            return node.getHullObjects();
        }
    };

    /**
     * Return a list of objects from the {@code node} specified by the merge type.
     * Result is used for building a new hull.
     *
     * @param node a node
     * @return a list of objects from the {@code node} specified by the merge type
     */
    abstract public List<LocalAbstractObject> getObjects(Node node);
}
