package mhtree;

/**
 * Specifies how new objects should be added to the MH-Tree.
 */
public enum InsertType {
    /**
     * When the inserted object is not covered by a node, all objects under such node are retrieved (recursively down to the buckets),
     * and a new hull is built from scratch, replacing the current one.
     * This strategy should create hulls that cover all data under it, and it forms a "baseline" solution.
     */
    GREEDY,

    /**
     * Take current hull objects and the newly inserted object together and compute a new hull out of them. 
     * This should do the "optimization" of an existing hull by replacing an existing hull object by the new object before enlarging the hull, as summarized below.
     * 
     * When the inserted object is not covered by node, we iterate over hull objects beginning with the nearest one.
     * We try to replace an existing hull object by replacing it with the inserted object.
     * If the removed hull object is covered by the new hull, we are done.
     * If no such hull object is found, the inserted object is simply added as a new hull object.
     */
    INCREMENTAL,

    /** 
     * Add the non-covered object as a new hull object, so the hull is enlarged by one object.
     */
    ADD_HULL_OBJECT,
}
