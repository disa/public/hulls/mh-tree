package mhtree.benchmarking;

import messif.objects.LocalAbstractObject;

import java.util.List;
import java.util.function.Consumer;

public class BenchmarkConfig {
    private final List<MHTreeConfig> mhTreeConfigs;
    private final List<Consumer<MHTreeConfig>> benchmarkFunctions;

    public BenchmarkConfig(List<MHTreeConfig> mhTreeConfigs, List<Consumer<MHTreeConfig>> benchmarkFunctions) {
        this.mhTreeConfigs = mhTreeConfigs;
        this.benchmarkFunctions = benchmarkFunctions;
    }

    public void execute(List<LocalAbstractObject> objects) {
        for (MHTreeConfig mhTreeConfig : mhTreeConfigs) {
            mhTreeConfig.setObjects(objects);

            for (Consumer<MHTreeConfig> benchmarkFunction : benchmarkFunctions) {
                benchmarkFunction.accept(mhTreeConfig);
                System.out.println();
            }
        }
    }
}
