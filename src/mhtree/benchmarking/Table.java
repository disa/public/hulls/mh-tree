package mhtree.benchmarking;

import java.util.Arrays;
import java.util.Map;

public class Table {
    String[] header;
    String formatString;

    Table(String[] header) {
        this.header = header;

        Arrays.stream(header).forEach(h -> formatString += "%-35s");
        formatString += "%n";

        System.out.format(formatString, (Object[]) header);
    }

    public void print(Map<String, Object> stats) {
        System.out.format(formatString, Arrays.stream(header).map(stats::get).toArray());
    }
}
