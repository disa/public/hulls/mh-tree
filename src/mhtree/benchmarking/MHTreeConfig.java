package mhtree.benchmarking;

import messif.objects.LocalAbstractObject;
import mhtree.InsertType;
import mhtree.ObjectToNodeDistance;

import java.util.List;

public class MHTreeConfig {
    public final int leafCapacity;
    public final int nodeDegree;
    public final InsertType insertType;
    public final ObjectToNodeDistance objectToNodeDistance;
    public List<LocalAbstractObject> objects;

    MHTreeConfig(int leafCapacity, int nodeDegree, InsertType insertType, ObjectToNodeDistance objectToNodeDistance) {
        this.leafCapacity = leafCapacity;
        this.nodeDegree = nodeDegree;
        this.insertType = insertType;
        this.objectToNodeDistance = objectToNodeDistance;
    }

    public void setObjects(List<LocalAbstractObject> objects) {
        this.objects = objects;
    }

    @Override
    public String toString() {
        return "MHTreeConfig{" +
                "leafCapacity=" + leafCapacity +
                ", numberOfChildren=" + nodeDegree +
                ", insertType=" + insertType +
                ", objectToNodeDistance=" + objectToNodeDistance +
                '}';
    }
}
