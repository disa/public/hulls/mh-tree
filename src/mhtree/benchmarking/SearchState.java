package mhtree.benchmarking;

import messif.objects.LocalAbstractObject;
import messif.operations.query.ApproxKNNQueryOperation;
import mhtree.ApproximateState;
import mhtree.MHTree;
import mhtree.ObjectToNodeDistanceRank;

import java.util.PriorityQueue;

public class SearchState extends SearchInfoState {
    public PriorityQueue<ObjectToNodeDistanceRank> queue;
    public LocalAbstractObject queryObject;
    public ApproximateState approximateState;

    public SearchState(MHTree tree, ApproxKNNQueryOperation operation) {
        this.queue = new PriorityQueue<>();
        this.queue.add(new ObjectToNodeDistanceRank(operation.getQueryObject(), tree.getRoot(), operation.getK()));
        this.queryObject = operation.getQueryObject();
        this.approximateState = ApproximateState.create(operation, tree);
    }
}
