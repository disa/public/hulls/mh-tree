package mhtree.benchmarking;

import java.util.Collections;
import java.util.List;

public class Stats {
    List<Double> sortedData;

    public Stats(List<Double> data) {
        sortedData = data;
        Collections.sort(sortedData);
    }

    public double getMin() {
        return sortedData.get(0);
    }

    public double getAverage() {
        return sortedData.stream().mapToDouble(x -> x).sum() / sortedData.size();
    }

    /**
     * @return Lower median
     */
    public double getMedian() {
        return sortedData.get((sortedData.size() + 1) / 2 - 1);
    }

    public double getMax() {
        return sortedData.get(sortedData.size() - 1);
    }

    @Override
    public String toString() {
        return String.format("%.2f %.2f(%.2f) %.2f", getMin(), getAverage(), getMedian(), getMax());
    }
}

