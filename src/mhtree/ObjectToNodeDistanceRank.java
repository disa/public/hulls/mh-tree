package mhtree;

import messif.objects.LocalAbstractObject;

import static mhtree.ObjectToNodeDistance.NEAREST;
import static mhtree.ObjectToNodeDistance.FURTHEST;

/**
 * Represents the rank of a node in the priority queue
 */
public class ObjectToNodeDistanceRank implements Comparable<ObjectToNodeDistanceRank> {

    private final Node node;
    private final double distance;

    public ObjectToNodeDistanceRank(LocalAbstractObject object, Node node, int k) {
        this.node = node;

        if (node.isLeaf()) {
            this.distance = NEAREST.getDistance(object, node);
            return;
        }

        if (k == 1) {
            if (node.isCovered(object)) {
                this.distance = -FURTHEST.getDistance(object, node);
            } else {
                this.distance = FURTHEST.getDistance(object, node);
            }
            return;
        }

        if (node.isCovered(object)) {
            this.distance = -NEAREST.getDistance(object, node);
        } else {
            this.distance = NEAREST.getDistance(object, node);
        }
    }

    @Override
    public int compareTo(ObjectToNodeDistanceRank rank) {
        return Double.compare(this.distance, rank.distance);
    }

    public Node getNode() {
        return node;
    }

    public double getDistance() {
        return distance;
    }
    
}
